// import * as twgl from 'twgl-full.js';
import _ from 'lodash';
import * as StackBlur from 'stackblur-canvas';

import { BLUR_RADIUS, TEX_SIZE } from './CONSTANTS';

const m4 = twgl.m4;
const mat = m4.identity();
const tmat = m4.identity();
let canvas2d, ctx, canvasGl, gl;
let images = [new Image(), new Image()];
let noiseImage = new Image();
let currentImage = 0;
let clicks = 0;
let programInfo;
let frameBufferOne, frameBufferTwo;
let attributeBuffer;
let texture, texturePrev, texutreNoise, texturePass;
let time = 0;
let isAnimating = false;

let raf, then, now, correction;


let uniforms = {
	matrix: mat,
	textureMatrix: tmat,
	texturePass,
	texutreNoise,
	texture,
	time,
	resolution: [0, 0],
	blur: 0,
	fade: 0,
	effectStrength: 0.,
};

// Implementation of canvas 2d drawImage to GL
const drawImage = (tex, texWidth, texHeight, srcX, srcY, srcWidth, srcHeight, dstX, dstY, dstWidth, dstHeight, targetWidth, targetHeight) => {
	m4.translate(tmat, [srcX / texWidth, srcY / texHeight, 0], tmat);
	m4.scale(tmat, [srcWidth / texWidth, srcHeight / texHeight, 1], tmat);

	m4.translate(mat, [-1, 1, 0], mat);
	m4.scale(mat, [2 / targetWidth, -2 / targetHeight, 1], mat); 

	m4.translate(mat, [dstX, dstY, 0], mat);
	m4.scale(mat, [dstWidth, dstHeight, 1], mat);
}

const render = () => {
	then = now ? now : null;
	now = new Date().getTime();
	correction = then ? (now - then) / 16.666 : 1;
	correction = Math.min(correction, 2) // so that it doesn't go too crazy sometimes if window loose focus
	
	
	time += 1 * correction;
	uniforms.time = time;
	twgl.setUniforms(programInfo, uniforms);

	
	// Here I want to do some blur post-processing
	// I'm using this for help: https://github.com/Jam3/glsl-fast-gaussian-blur/blob/master/demo/index.js
	// Maybe it's something like the code below, although I know there's some stuff missing here!

	let writeBuffer = frameBufferOne;
	let readBuffer = frameBufferTwo;
	// set it to draw into the write buffer
	uniforms.blur = 0;
	twgl.setUniforms(programInfo, uniforms);
	twgl.bindFramebufferInfo(gl, writeBuffer);
	twgl.drawBufferInfo(gl, gl.TRIANGLES, attributeBuffer);
	
	let tmp = writeBuffer;
	writeBuffer = readBuffer;
	readBuffer = tmp;
	
	if (window.location.search.indexOf('no-blur') === -1) {
		for (let i = 1; i <= BLUR_RADIUS; i+=1) {
			uniforms.blur = i;
			
			uniforms.blurDirection = i % 2 ? [0, 1] : [1, 0];
			uniforms.u_texture = readBuffer.attachments[0];
			uniforms.texturePass = readBuffer.attachments[0];

			twgl.setUniforms(programInfo, uniforms);
			twgl.bindFramebufferInfo(gl, writeBuffer);
			twgl.drawBufferInfo(gl, gl.TRIANGLES, attributeBuffer);

			tmp = writeBuffer;
			writeBuffer = readBuffer;
			readBuffer = tmp;
		}
	}

	twgl.setUniforms(programInfo, uniforms);
	twgl.bindFramebufferInfo(gl, null);
	twgl.drawBufferInfo(gl, gl.TRIANGLES, attributeBuffer);

	// TODO: Create the fully blurred image, and the none-blurred image
	// Then in the shader compose the two images to create a sharp line in the blur
	requestAnimationFrame(render);
}

// When the image loads, draw it and start the render loop
const onImageLoaded = () => {
	uniforms.texturePass = twgl.createTexture(gl, { width: ctx.canvas.width, height: ctx.canvas.height });
	ctx.drawImage(images[0], 0, 0, canvas2d.width, canvas2d.height);
	uniforms.texture = twgl.createTexture(gl, { src: ctx.canvas });
	console.log(uniforms.texture);

	requestAnimationFrame(() => {
		drawImage(
			texture,
			canvas2d.width, //texWidth
			canvas2d.height, //texHeight
			0, 0, //srcX, srcY
			canvas2d.width, //srcWidth
			canvas2d.height, //srcHeight
			0, 0, //dstX, dstY
			gl.canvas.width, //dstWidth
			gl.canvas.height, //dstHeigh
			gl.canvas.width, //targetWidth
			gl.canvas.height //targetHeight
		);
		render();
	})
}

// Swaps the image from old to new
const swapImage = () => {
	if (isAnimating) return;
	clicks++;
	isAnimating = true;
	uniforms.fade = 0;

	const imageIncoming = clicks % 2 === 0 ? images[0] : images[1];
	const imageOutgoing = clicks % 2 === 0 ? images[1] : images[0];

	uniforms.texturePrev = undefined;
	uniforms.texturePrev = undefined;

	uniforms.texturePrev = twgl.createTexture(gl, { src: ctx.canvas });
	ctx.drawImage(imageIncoming, 0, 0, ctx.canvas.width, ctx.canvas.height);
	uniforms.texture = twgl.createTexture(gl, { src: ctx.canvas });
	
	const loop = () => {
		uniforms.fade += 0.033;
		uniforms.fade = Math.min(uniforms.fade, 1);
		if (uniforms.fade < 1) return requestAnimationFrame(loop);
		return isAnimating = false;
	}

	loop();
}

// const testSwapImage = () => {
// 	swapImage();
// 	setTimeout(testSwapImage, 5555);
// }

export const init = () => {
	const m4 = twgl.m4;
	canvasGl = document.querySelector(".canvas--gl");
	canvasGl.width = canvasGl.clientWidth * window.devicePixelRatio;
	canvasGl.height = canvasGl.clientHeight * window.devicePixelRatio;

	canvas2d = document.querySelector(".canvas--2d");
	canvas2d.width = TEX_SIZE;
	canvas2d.height = TEX_SIZE;
	ctx = canvas2d.getContext("2d");

	const options = {
		alpha: false,
		antialias: true,
		depth: false,
		failIfMajorPerformanceCaveat: false,
		premultipliedAlpha: false,
		preserveDrawingBuffer: false,
		stencil: false,
	};

	gl = twgl.getWebGLContext(canvasGl, options);
	gl.clearColor(1, 1, 1, 1);
	const MAX_TEXTURE_SIZE = gl.getParameter(gl.MAX_TEXTURE_SIZE);
	console.log(Math.sqrt(MAX_TEXTURE_SIZE), '<<');
	programInfo = twgl.createProgramInfo(gl, [window.app.shaders.vert, window.app.shaders.frag]);

	// a unit quad
	const arrays = {
		position: { 
			numComponents: 2, 
			data: [
				0, 0,  
				1, 0, 
				0, 1, 
				0, 1, 
				1, 0,  
				1, 1,
			],
		},
	};
	// Create attribute buffers
	attributeBuffer = twgl.createBufferInfoFromArrays(gl, arrays);

	// Create frame buffers
	frameBufferOne = twgl.createFramebufferInfo(gl, null, canvas2d.width, canvas2d.height);
	frameBufferTwo = twgl.createFramebufferInfo(gl, null, canvas2d.width, canvas2d.height);

	texture = twgl.createTexture(gl, { src: ctx.canvas });
	uniforms.textureNoise = twgl.createTexture(gl, { src: 'assets/noise--small.jpg' });
	uniforms.resolution = [canvas2d.width, canvas2d.height];

	twgl.resizeCanvasToDisplaySize(gl.canvas);
   	gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
   	gl.useProgram(programInfo.program);
   	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	twgl.setBuffersAndAttributes(gl, programInfo, attributeBuffer);

	images[0].onload = onImageLoaded;
	images[0].src = 'assets/shoe-test.jpg';
	images[1].src = 'assets/shoe-test--2.jpg';

	canvasGl.addEventListener('click', _.throttle(swapImage, 999));
}