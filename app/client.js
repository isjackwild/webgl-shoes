import { init as initDrawing } from './drawing';

window.app = window.app || {};
window.app.shaders = {};


const assetsToLoad = 2;
let assetsLoaded = 0;

const onLoaded = () => {
	console.log('on loaded');
	initDrawing();
}

const loadAssets = () => {
	fetch('assets/shaders/frag.glsl')
	.then(function(response) {
		return response.text();
	})
	.then(function(text) {
		window.app.shaders.frag = text;
		assetsLoaded++;
		if (assetsLoaded === assetsToLoad) onLoaded();
	});

	fetch('assets/shaders/vert.glsl')
	.then(function(response) {
		return response.text();
	})
	.then(function(text) {
		window.app.shaders.vert = text;
		assetsLoaded++;
		if (assetsLoaded === assetsToLoad) onLoaded();
	});
}

const kickIt = () => {
	loadAssets();
}

const onResize = () => {
	window.app.width = window.innerWidth;
	window.app.height = window.innerHeight;
}

const addEventListeners = () => {
	window.addEventListener('resize', _.debounce(onResize, 99));
}


if (document.addEventListener) {
	document.addEventListener('DOMContentLoaded', kickIt);
} else {
	window.attachEvent('onload', kickIt);
}