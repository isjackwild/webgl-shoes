precision highp float;

varying vec2 texcoord;
uniform sampler2D u_texture;
uniform sampler2D texturePass;
uniform sampler2D texture;
uniform sampler2D texturePrev;
uniform sampler2D textureNoise;
uniform vec2 resolution;
uniform float effectStrength;
uniform float fade;
uniform float time;
uniform float blur;
uniform vec2 blurDirection;


mat2 rotate2d (float angle) {
	return mat2(
		cos(angle),
		sin(angle) * -1.0,
		sin(angle),
		cos(angle)
	);
}

vec4 blur5(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
	vec4 color = vec4(0.0);
	vec2 off1 = vec2(1.3333333333333333) * direction;
	color += texture2D(image, uv) * 0.29411764705882354;
	color += texture2D(image, uv + (off1 / resolution)) * 0.35294117647058826;
	color += texture2D(image, uv - (off1 / resolution)) * 0.35294117647058826;
	return color; 
}

vec4 blur9(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
	vec4 color = vec4(0.0);
	vec2 off1 = vec2(1.3846153846) * direction;
	vec2 off2 = vec2(3.2307692308) * direction;
	color += texture2D(image, uv) * 0.2270270270;
	color += texture2D(image, uv + (off1 / resolution)) * 0.3162162162;
	color += texture2D(image, uv - (off1 / resolution)) * 0.3162162162;
	color += texture2D(image, uv + (off2 / resolution)) * 0.0702702703;
	color += texture2D(image, uv - (off2 / resolution)) * 0.0702702703;
	return color;
}

vec4 blur13(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
	vec4 color = vec4(0.0);
	vec2 off1 = vec2(1.411764705882353) * direction;
	vec2 off2 = vec2(3.2941176470588234) * direction;
	vec2 off3 = vec2(5.176470588235294) * direction;
	color += texture2D(image, uv) * 0.1964825501511404;
	color += texture2D(image, uv + (off1 / resolution)) * 0.2969069646728344;
	color += texture2D(image, uv - (off1 / resolution)) * 0.2969069646728344;
	color += texture2D(image, uv + (off2 / resolution)) * 0.09447039785044732;
	color += texture2D(image, uv - (off2 / resolution)) * 0.09447039785044732;
	color += texture2D(image, uv + (off3 / resolution)) * 0.010381362401148057;
	color += texture2D(image, uv - (off3 / resolution)) * 0.010381362401148057;
	return color;
}

void main() {
	float timeScaled = time * 0.015;
	vec2 pos = texcoord;
	vec4 fragColor = vec4(0.0);

	if (blur == 0.0) { // If this is the first pass, u_blur will be 0, and so render the pass for the animated distortion and texture fades

		// Apply the distortion
		float n = texture2D(textureNoise, pos * 0.3 + timeScaled * 0.06).r;
		n -= 0.5;
		n *= (0.15 * effectStrength);

		float n1 = texture2D(textureNoise, pos * 0.3 + timeScaled * 0.06).r;
		n1 -= 0.5;
		n1 *= (0.3 * effectStrength);

		float n2 = texture2D(textureNoise, pos * 0.3 + timeScaled * 0.06).r;
		n2 -= 0.5;
		n2 *= (0.15 * effectStrength);

		pos *= rotate2d(n);
		pos.x += n1;
		pos.y += n2;

		vec4 color = vec4(0.0);
		vec4 colorPrev = vec4(0.0);

		color = texture2D(texture, pos);
		colorPrev = texture2D(texturePrev, pos);

		// Fade between the current and previous texture, when the texture is updated
		fragColor = mix(colorPrev, color, fade);
	} else { // If this is NOT the first pass, u_blur will be > 0, and so render should blur the image from the previous pass

		// Apply the blur
		// vec2 resolution = vec2(650.0 * 2.0, 360.0 * 2.0);
		if (texcoord.x < 0.5) {
			// vec2 direction = vec2(0.0, blur);
			vec2 blurVector = blurDirection * blur;
			fragColor = blur9(texturePass, pos, resolution, blurVector);
			// direction = vec2(blur, 0.0);
			// fragColor = blur9(u_texture, pos, resolution, direction);
		} else {
			fragColor = texture2D(texturePass, pos);
		}
	}

	gl_FragColor = fragColor;
}