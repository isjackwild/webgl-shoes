precision highp float;

varying vec2 texcoord;
uniform sampler2D u_texture;
uniform sampler2D texture;
uniform sampler2D texturePrev;
uniform sampler2D textureNoise;
uniform float fade;
uniform float time;
uniform float blur;


// NOISE STUFF
float random (in vec2 st) {
	return fract(sin(dot(st.xy, vec2(12.9898, 78.233))) * 43758.5453123);
}

// Value noise by Inigo Quilez - iq/2013
// https://www.shadertoy.com/view/lsf3WH
float valNoise (vec2 st) {
	vec2 i = floor(st);
	vec2 f = fract(st);
	vec2 u = f * f * (3.0 - 2.0 * f);

	return mix(
		mix(
			random(i + vec2(0.0, 0.0)),
			random(i + vec2(1.0, 0.0)),
			u.x
		),
		mix(
			random(i + vec2(0.0, 1.0)),
			random(i + vec2(1.0, 1.0)),
			u.x
		),
		u.y
	);
}

// 2D Noise based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd
float noise (in vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));

    // Smooth Interpolation

    // Cubic Hermine Curve.  Same as SmoothStep()
    vec2 u = f*f*(3.0-2.0*f);
    // u = smoothstep(0.,1.,f);

    // Mix 4 coorners porcentages
    return mix(a, b, u.x) +
            (c - a)* u.y * (1.0 - u.x) +
            (d - b) * u.x * u.y;
}

mat2 rotate2d (float angle) {
	return mat2(
		cos(angle),
		sin(angle) * -1.0,
		sin(angle),
		cos(angle)
	);
	// return mat2(1.0);
}

vec4 blur9(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
	vec4 color = vec4(0.0);
	vec2 off1 = vec2(1.3846153846) * direction;
	vec2 off2 = vec2(3.2307692308) * direction;
	color += texture2D(image, uv) * 0.2270270270;
	color += texture2D(image, uv + (off1 / resolution)) * 0.3162162162;
	color += texture2D(image, uv - (off1 / resolution)) * 0.3162162162;
	color += texture2D(image, uv + (off2 / resolution)) * 0.0702702703;
	color += texture2D(image, uv - (off2 / resolution)) * 0.0702702703;
	return color;
}

void main() {
	float timeScaled = time * 0.015;
	vec2 pos = texcoord;

	// TODO: Use a noise texture.
	// float n = cos(pos.x * 4.0 + timeScaled * 0.5) * sin(pos.x * 4.0 + (timeScaled * 0.9));
	// float n = valNoise(pos * 5.0 + timeScaled);
	float n = texture2D(textureNoise, pos * 0.3 + timeScaled * 0.06).r;
	n -= 0.5;
	n *= 0.15;

	// float n1 = valNoise(pos * 4.0 + timeScaled + 100.0);
	// float n1 = cos(pos.x * 4.0 + timeScaled + 100.0);
	float n1 = texture2D(textureNoise, pos * 0.3 + timeScaled * 0.06).r;
	n1 -= 0.5;
	n1 *= 0.3;

	// float n2 = sin(pos.y * 4.0 + (timeScaled * 1.1) + 200.0);
	float n2 = texture2D(textureNoise, pos * 0.3 + timeScaled * 0.06).r;
	n2 -= 0.5;
	n2 *= 0.15;

	pos *= rotate2d(n);
	pos.x += n1;
	pos.y += n2;
	// vec2 pos = texcoord + n;

	vec2 resolution = vec2(650.0 * 2.0, 360.0 * 2.0);
	vec2 direction = vec2(blur, 0.0);

	vec4 color = vec4(0.0);
	vec4 colorPrev = vec4(0.0);

	// if (texcoord.x < 0.5) {
	// 	color = blur9(texture, pos, resolution, direction);
	// 	colorPrev = blur9(texturePrev, pos, resolution, direction);
	// } else {
	// }
	color = texture2D(texture, pos);
	colorPrev = texture2D(texturePrev, pos);

	vec4 fragColor = mix(colorPrev, color, fade);
	gl_FragColor = fragColor;
}